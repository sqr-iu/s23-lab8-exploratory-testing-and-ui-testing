# Testcases

## "Search and Documentation" works

| Step Number | What Done                                                        | Status | Comment                                                                                                                      |
|-------------|------------------------------------------------------------------|--------|------------------------------------------------------------------------------------------------------------------------------|
| 1           | Navigate to https://hoogle.haskell.org/                          | Pass   | The page loaded successfully.                                                                                                |
| 2           | Verify the presence of the search bar                            | Pass   | Search bar is visible on the page.                                                                                           |
| 3           | Fill the search bar with "filter" and click first link           | Pass   | Inputs "filter" into the search bar and submits the search query.                                                            |
| 4           | Verify the presence of the search bar on the results page        | Pass   | Search bar is still visible on the search results page, indicating that the user is still able to search for more functions. |
| 5           | Click on the first function result                               | Pass   | Clicks on the first function result in the search results to navigate to the corresponding package page.                     |
| 6           | Verify that we are now at the page of package with this function | Pass   | URL of the current page matches the URL of the package page.                                                                 |

## "GHC Manual Copyright" link works

| Step Number | What Done                                                         | Status | Comment                                                       |
|-------------|-------------------------------------------------------------------|--------|---------------------------------------------------------------|
| 1           | Navigate to https://hoogle.haskell.org/                           | Pass   | The page loaded successfully.                                 |
| 2           | Verify the presence of the "Links" section                        | Pass   | The "Links" section is present on the homepage.               |
| 3           | Click on the "GHC Manual" link                                    | Pass   | Clicked on the "GHC Manual" link in the "Links" section.      |
| 4           | Verify that we are now on the GHC Manual page                     | Pass   | The page URL matches the expected URL of the GHC Manual page. |
| 5           | Scroll to the bottom of the page                                  | Pass   | Successfully scrolled to the bottom of the page.              |
| 6           | Verify the presence of the "© Copyright 2020, GHC Team" copyright | Pass   | The "© Copyright 2020, GHC Team" text is present on the page. |

## "Hackage" link works

| Step Number | What Done                                              | Status | Comment                                                                     |
|-------------|--------------------------------------------------------|--------|-----------------------------------------------------------------------------|
| 1           | Navigate to https://hoogle.haskell.org/                | Pass   | The page loaded successfully.                                               |
| 2           | Verify the presence of the "Hackage" link              | Pass   | The "Hackage" link is present in the top of the page.                       |
| 3           | Click on the "Hackage" link                            | Pass   | Clicked on the "Hackage" link and was redirected to the Hackage main page.  |
| 4           | Search for a package                                   | Pass   | Searched for the package "hspec" and the results were displayed accurately. |
| 5           | Click on the package name to view its details          | Pass   | Clicked on the "hspec" link in the table to view its details.               |
| 6           | Verify the presence of the package's modules structure | Pass   | The package's modules structure is displayed accurately on the page.        |
