from playwright.sync_api import Page


def remove_version_from_url(url):
    url_parts = url.split("/")
    pkg_name = url_parts[4].split("-")[0]
    url_parts[4] = pkg_name
    return "/".join(url_parts)


def test_search_and_documentation(page: Page):
    # Step 1: Navigate to https://hoogle.haskell.org/
    page.goto("https://hoogle.haskell.org/")

    # Step 2: Verify the presence of the search bar
    search_bar = page.locator("input[name=hoogle]")
    assert search_bar.is_visible(), "Search bar is not visible."

    # Step 3: Fill the search bar with "filter" and click first link
    search_bar.fill("filter")
    page.click("input#submit")
    page.wait_for_selector(".ans", timeout=3000)

    # Step 4: Verify the presence of the search bar on the results page
    search_bar = page.locator("input[name=hoogle]")
    assert search_bar.is_visible(), "Search bar is not visible on the results page."

    # Step 5: Click on the first function result
    link = page.locator('xpath=//*[@id="body"]/div[1]/div[1]/a')
    href = link.get_attribute("href")
    link.click()

    # Step 6: Verify that we now at page of package with this function
    assert remove_version_from_url(page.url) == href, "URLs are not match"
